
def file(s):
    with open(s,mode="r",encoding="utf-8") as f:
        return f.read()

def wfile(fname,s):
    with open(fname,mode="w",encoding="utf-8") as f:
        f.write(s)

def angou(s):
    ret=[]
    for c in s:
        ret.append(str(ord(c)))
    return "#".join(ret)

if __name__=="__main__":
    f_in=input("暗号化したいファイルを入力>>")
    wfile(f_in+".ang",angou(file(f_in)))

