#!/bin/bash
beat_mario_enable="source ~/shell-tools/beat_mario/.libs/_enable.sh"
beat_mario_disable="source ~/shell-tools/beat_mario/.libs/_disable.sh"
alias bme="$beat_mario_enable"
alias bmd="$beat_mario_disable"
beat_mario_command=false
declare -A beat_mario_processes
source ~/shell-tools/beat_mario/.libs/winapedia.sh
