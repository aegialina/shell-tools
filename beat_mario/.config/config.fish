set bm "beat_mario"
alias beat_mario_enable="source ~/$bm/.libs/=enable.fish"
alias beat_mario_disable="source ~/$bm/.libs/=disable.fish"
alias bme="beat_mario_enable"
alias bmd="beat_mario_disable"
set beat_mario_command false