#!/bin/bash
#beat_mario command can use eat
bm="shell-tools/beat_mario"
alias python="python3"
tfm="the-fighter-marcia"
function __blook_func
{
    ls ./build/*.soft
}
function __execute_func
{
    ./build/*.soft $@
}
alias blook="__blook_func"
alias exe="__execute_func"
function dotnet-path
{
    self=`arch`
    mkdir -p ~/dotnet && tar zxf dotnet-sdk-5.0.302-linux-${self}.tar.gz -C ~/dotnet
    export DOTNET_ROOT=~/dotnet
    export PATH=$PATH:~/dotnet
}

function dir()
{
    local argc=$#
    if [ "$argc" -ge 1 ]
        then
        cd ~/$bm/$1
    else
        cd ~/$bm
    fi
    pwd
}

function sf1()
{
    soft=`find ./ -name "*.soft"`
    if [ $soft == "" ]
        then
        soft=`find ./build -name "*.soft"`
    fi
    echo $soft
}

function sf()
{
    soft=`sf1`
    chmod u+x ${soft}
    if [ $# -eq 0 ]
        then
        ${soft}
    else
        ${soft} $@[1:$#]
    fi
}

function pybuild()
{
    python3 build/setup.py build_ext -i
    mv Binaural.* build
}
function __builds_build__
{
    ./build/build.sh
}
alias builds="__builds_build__"
alias proj="./prj.sh"
alias lsa="ls -AF"
alias cfg-code="code ~/.config/Code/User/settings.json"
alias code-arg="code ~/.vscode/argv.json"
alias edit-enable="code ~/$bm/.libs/_enable.sh"
alias edit-disable="code ~/$bm/.libs/_disable.sh"
alias edits="edit-enable;edit-disable"
alias ug="sudo apt upgrade"
alias ud="sudo apt update"
alias ug-ok="sudo apt upgrade -y"
alias ugc="ug-ok code"
alias eat="~/$bm/program-files/beat_mario.soft"
alias eirin="eat --help --me"
alias eirn="eirin"
alias ern="eirin"
alias stn="e--list"
alias ssh-key="cd ~/.ssh;ls"
alias want="sudo apt install"
alias want-ok="sudo apt -y install"
alias cl-set="code ./cl.ern"
function tg()
{
    tar -zcvf $1.tar.gz ./$1
}
alias chdir="cd -L"
alias airty="__the_out_of"
pf="program-files"
alias __the_out_of=`$@`
alias destroy="sudo apt purge"
alias destroy-ok="sudo apt purge"
alias sln="ln -s"
alias audio3d="cd ~/spatial;source ~/spatial/prj.sh"
function __untg_of_func()
{
    tar -zxvf $1.tar.gz 
}
alias untg="__untg_of_func"
function tb(){
    tar -jcvf $1.tar.gz ./$1
}
function __untb_of_func()
{
    tar -jxvf $1.tar.bz2
}
function tx()
{
    tar -Jcvf $1.tar.xz ./$1
}
function untx()
{
    tar -Jxvf $1.tar.xz
}
alias untb="__untb_of_func"
function __this_is_opt()
{
    string_test=$1
    if [ "${string_test:0:2}" = "--" ]
        then 
        echo 2
    elif [ "${string_test:0:1}" = "-" ]
        then
        echo 1
    else
        echo 0
    fi 
}

function __compling_for_clang()
{
    opts=`__this_is_opt $1`
    #echo "#$opts#"
    if [ $# -eq 1 -a $opts -le 1 ]
        then
        if [ "$1" = "-c" ]
            then
            make -f make.sfract clean
        elif [ "$1" = "-e" ]
            then
            code ./make.sfract
        else
            echo "error"
        fi
    elif [ $# -eq 1 ]
        then
        clang++ -std=c++17 $1.cpp
        if [ $? -eq 0 ]
            then
            mv a.out $1.soft
        fi
    elif [ $# -eq 0 ]
        then
        echo "done"
        make -f make.sfract
    else
        clang++ -std=c++17 ${@:1}
    fi
}
function __setting()
{
    # [1,2,3,"abc"=4]:動的配列及び連想配列
    # 1:数値
    # "abcd":文字列
    # true:boolean
    # undefined:null値。基本的に無視される
    # #:コメント
    local beat_mario_for_tip=""
    while IFS= read -r line; do
        if [ line:0:1 -eq "#" ]
            then
            continue
        fi
        local list=(${line/=/})
        if [ list[1]:0:1 -eq "[" ]
            then
            awk -F "[¥[]"
        fi
        beat_mario_for_tip=list[0]
        $beat_mario_for_tip=list[1]
    done < $1.ern
}
function __cl_dot_eirin()
{
    __setting ./cl
    echo "Num:"
}
function __stream_for_zip()
{
    echo "done areisiana"
    if [ $1 -eq "--zip" ]
    then
        PTR="zip"
    fi
}
function __stream_for_unzip()
{
    echo "done un-areisiana"
}
function __neet_for_eirin()
{
    echo "nt $1"
}
function __active_neet_for_eirin()
{
    echo "act nt $1"
}
function __ccode()
{
    code $1.cpp    
}
function __hcode()
{
    code $1.h
}
alias arein="__steram_for_zip"
alias arn="arein"
alias un-arein="__stream_for_unzip"
alias unarn="un-arein"
alias neet="__neet_for_eirin"
alias act-nt="__active_neet_for_eirin"
alias cl="__compling_for_clang"
alias ern-cl="__cl_dot_eirin"
alias ccode="__ccode"
alias udk="source ~/$bm/.libs/enable/basecom.sh"
alias edits-udk="code ~/$bm/.libs/enable/basecom.sh;code ~/$bm/.libs/disable/basecom.sh"
alias hcode="__hcode"
alias done-new-enable="source ~/新規作成/come-on.sh"
alias dne="done-new-enable"
alias knn="done-new-enable"
alias done-git-enable="source ~/$bm/.libs/enable/git.sh"
alias use-git="done-git-enable"
alias dge="done-git-enable"
alias edits-git="code ~/$bm/.libs/enable/git.sh;code ~/$bm/.libs/disable/git.sh"
alias edit-news="code ~/新規作成/come-on.sh"
alias mrs="source ~/$bm/.config/config.sh"
beat_mario_command=true
