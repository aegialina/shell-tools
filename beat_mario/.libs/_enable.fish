#beat_mario command can use eat
set bm "beat_mario"
function dir
    if string length -q -- $argv[1]
        cd ~/$bm/$argv[1]
    else
        cd ~/$bm
    end
end
alias lst="ls -A"
alias edit-enable="code ~/$bm/.libs/=enable.fish"
alias edit-disable="code ~/$bm/.libs/=disable.fish"
function __fn_ln_object_python
    python3 ./$argv[1]
end

alias opy="__fn_ln_object_python"
alias edits="edit-enable;edit-disable"
alias ug="sudo apt upgrade"
alias ud="sudo apt update"
alias ug-ok="sudo apt upgrade -y"
alias ugc="ug code"
alias eat="~/$bm/$pf/$bm.soft"
alias eirin="eat --help --me"
alias eirn="eirin"
alias ern="eirin"
alias stn="e--list"
alias ssh-key="cd ~/.ssh;ls"
alias want="sudo apt install"
alias want-ok="sudo apt -y install"
alias cl-set="code ./cl.ern"
alias tg="tar -zcvf"
alias chdir="cd -L"
alias destroy="sudo apt purge"
alias destroy-ok="sudo apt purge"
alias sln="ln -s"
#alias airty=""

function __compling_for_clang
    if [ count $argv -eq 1 ]
        clang++ -std=c++17 $1.cpp
        if [ $status -eq 0 ]
            mv a.out $1.soft
        end
    else if [ count $argv -eq 0 ]
        clang++ -std=c++17 soft.cpp
        if [ $status -eq 0 ]
            mv a.out soft.soft
        end
    else
        echo "eirn"
    end
end

function __setting
    set beat_mario_for_tip ""
    while set IFS read -r line
        if [ line:0:1 -eq "#" ]
            continue
        end
        set list string split -m1 "=" $line
        if [ list[1]:0:1 -eq "[" ]
            awk -F "[¥[]"
        end
        set beat_mario_for_tip list[0]
        set $beat_mario_for_tip list[1]
    end < $1.ern
end
function __cl_dot_eirin
    __setting ./cl
    echo "Num:"
end
function __stream_for_zip
    echo "done areisiana"
    if [ $1 -eq "--zip" ]
        PTR="zip"
    end
end
function __stream_for_unzip
    echo "done un-areisiana"
end
function __neet_for_eirin
    echo "nt $1"
end
function __active_neet_for_eirin
    echo "act nt $1"
end
function __ccode
    code $1.cpp    
end
function __hcode
    code $1.h
end
alias arein="__steram_for_zip"
alias arn="arein"
alias un-arein="__stream_for_unzip"
alias unarn="un-arein"
alias neet="__neet_for_eirin"
alias act-nt="__active_neet_for_eirin"
alias cl="__compling_for_clang"
alias ern-cl="__cl_dot_eirin"
alias ccode="__ccode"
alias udk="source ~/$bm/.libs/enable/basecom.fish"
alias edits-udk="code ~/$bm/.libs/enable/basecom.fish;code ~/$bm/.libs/disable/basecom.fish"
alias hcode="__hcode"
alias done-new-enable="source ~/新規作成/come-on.fish"
alias dne="done-new-enable"
alias knn="done-new-enable"
alias dge="source ~/$bm/.libs/enable/git.fish"
alias edits-git="code ~/$bm/.libs/enable/git.fish;code ~/$bm/.libs/disable/git.fish"
alias edit-news="code ~/新規作成/come-on.fish"
alias mrs="source ~/beat_mario/.config/config.sh"
set beat_mario_command true
set -l pf "program-files"
