#!/bin/bash

function get-code
{
    want-ok curl
    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
    sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo rm microsoft.gpg
    this=`arch`
    vscode="deb [arch=${this}] https://packages.microsoft.com/repos/vscode stable main"
    sudo sh -c 'echo $vscode > /etc/apt/sources.list.d/vscode.list'
    want-ok apt-transport-https
    ud
    want-ok code
}

function set-config
{
    want-ok gnome-keyring
    get-code
    read -p "visual studio code ok (y/N): " yn
    sudo apt -y update
    want-ok fcitx-mozc
    want-ok task-japansese
    echo "serch j to ja_JP.UTF8"
    sudo dpkg-reconfigure locales
    sudo localectl set-locale LANG=ja_JP.UTF8 LANGAGE="ja_JP:ja"
    source /etc/default/local
    echo "
    Environment=''&name''
    name='GTK_IM_MODULE=fcitx'
    name='QtT_IM_MODULE=fcitx'
    name='XMODIFIERS=@im=fcitx'
    name='GDK_BACKEND=x11'
    "
    code /etc/systemd/user/cros-garcon.service.d/cros-garcon-override.conf
    read -p "config ok? (y/N): " yn
    echo '/usr/bin/fcitx-autostart'>>~/.sommelierrc

}

function get-softs
{
    bme
    read -p "start install config soft ok? (y/N): " yn
    case "$yn" in [yY]*) ;; *) echo "abort." ; return ;; esac
    set-config
    read -p "start install linux soft ok? (y/N): " yn
    case "$yn" in [yY]*) ;; *) echo "abort." ; return ;; esac
    want-ok gimp
    want-ok inkscape
    want-ok fish
    want-ok clang
    want-ok make
    want-ok jq
    want-ok xclip
    want-ok anjuta
    chmod u+x ../.libs/_enable.sh
    chmod u+x ../.libs/_disable.sh
}

source ~/shell-tools/beat_mario/.config/config.sh
get-softs
