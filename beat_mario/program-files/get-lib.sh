#!/bin/bash
function openal()
{
    bme
    want-ok libopenal-dev
    want-ok libalut0
    want-ok python3-dev
}

function get-js()
{    
    # Node.js と npm をインストール
    want-ok nodejs npm
    # npm で n をインストールして Node.js を安定版に
    sudo npm install n -g
    sudo n stable
    # aptでインストールしたNodeを削除
    sudo apt purge nodejs npm
    sudo npm install showdown
}

source ~/shell-tools/beat_mario/.config/config.sh
