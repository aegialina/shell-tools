import math

def reverse(num):
    if num==0:
        return -1
    return int(math.log10((num*9+7)/34))

def common(n):
    return int((34*10**n-7)/9)

def isPrime(x):
    if x == 1:
        return False
    if x == 2:
        return True
    for i in range(2, int(x**0.5) + 1):
        if x % i == 0:
            return False
    return True

def baseFromIsPrime(x):
    return isPrime(common(x))

